# PictureApi

> 需要头像，背景随机调用接口？<br />
> 没有调用接口？<br /> 
> 别人的不放心，图片不好看？<br />
> 现在用微晨PictureApi给自己搭建API调用<br />

### 简介
微晨PictureApi是一款图片，背景随机调用平台

### 安装教程

1.  下载源码
2.  上传至你的服务器目录，让后解压
3.  打开你相应的域名进行确认
4.  演示站点：[tu.wcst.xyz](http://tu.wcst.xyz)

### 使用说明

1.  /picture/touxiang/这是放头像图片调用的目录
2.  /picture/beijing/这是放背景图片调用的目录
3.  建议不要修改文件里的网址和文件名，因为许多文件是相互对应的
4.  有建议或者意见可以在[blog.wcst.xyz/about.html](http://blog.wcst.xyz/about.html)留言
5.  同时在login.php与header.php文件你修改后台密码，login.php与header.php的密码必须相同！

### 版权©

（微晨）WCST版权所有 改源码依据[Apache2](https://www.apache.org/licenses/LICENSE-2.0.html)开源协议开源,请不要修改版权

### 更新
测试版本v1.0（ **已删除** ）：内容不多，但很精致！随后将会更新v1.1版本<br />
v测试版本1.1（ **已删除** ）：更新后台登录和图片快捷上传<br />
测试版本v1.2（ **已删除** ）：去除用数据库里的密码登录，同时在login.php与header.php文件你修改后台密码，login.php与header.php的密码必须相同！<br />
正式版本v1.0：经过充分测试可以完全使用，有后台，图片快捷上传等，具体请体验