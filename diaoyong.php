<!--
WCST版权所有 | 盗版必究
http://blog.wcst.xyz
V1.0
2021/06/8
-->
<?php require_once "header.php"; ?>
<div class="mdui-container doc-container">
    <div class="mdui-typo">
        <h2>帮助</h2>
        1.本站网址：http://<?php echo $_SERVER['HTTP_HOST']?><br />
        2.客服QQ：2839472271<br />
        3.本站支持随机头像，随机竖屏背景，两种图片调用<br />
        
    </div>
</div>
<div class="mdui-container doc-container">
    <div class="mdui-typo">
        <h2>图片API接口</h2>
        <div class="mdui-table-fluid">
            <table class="mdui-table mdui-table-hoverable">
                <tbody>
                    <tr>
                        <td>随机头像调用地址</td>
                        <td>
                            http://<?php echo $_SERVER['HTTP_HOST']?>/apitx.php</td>
                    </tr>
                    <tr>
                        <td>随机背景调用地址</td>
                        <td>
                            http://<?php echo $_SERVER['HTTP_HOST']?>/apibj.php</td>
                    </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?php require_once "footer.php"; ?>