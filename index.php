<?php require_once "header.php"; ?>
<div class="mdui-container">
    <div class="mdui-typo">
         <h2 class="doc-chapter-title doc-chapter-title-first">欢迎来到微晨API调用</h2>
        <blockquote>
            <div class="mdui-typo-title-opacity">随机头像背景调用</div>
            <footer>原谅我这一生，不羁放纵爱自由</footer>
        </blockquote>
    </div>
    <br />&emsp;<a href="diaoyong.php" class="mdui-btn mdui-btn-dense mdui-color-theme-accent mdui-ripple"><i class="mdui-icon material-icons"></i>&emsp;开始调用</a>
    <br />
    <div class="mdui-typo">
        <blockquote>
            <p>没有好看的随机头像？
                <br />没有好看的随机背景图片？
                <br />国外调用经常被墙。
                <br />现在用微晨API调用</p>
            <footer>好看的随机头像和背景</footer>
        </blockquote>
    </div>
</div>
<?php
require_once "footer.php";
?>