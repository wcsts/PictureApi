<!--
WCST版权所有 | 盗版必究
http://blog.wcst.xyz
V1.0
2021/06/8
-->
<title>背景上传 - 微晨API调用</title>   
<?php
require_once "./header.php";
?>
  <div class="mdui-toolbar mdui-color-theme">
    <a class="mdui-typo-title">背景快捷上传(您有<?php
function ShuLiang($url)
{
    $sl=0;
    $arr = glob($url);
    foreach ($arr as $v)
    {
        if(is_file($v))
        {
            $sl++;
        }
        else
        {
            $sl+=ShuLiang($v."/*");
        }
    }
    return $sl;
}
echo ShuLiang("../picture/beijing/");
?>张背景图片)</a>
  </div>
<br />
<div class="mdui-container doc-container" style='max-width:85%'>
  <div class="mdui-typo">
    <div class="mdui-table-fluid">
      <table class="mdui-table">
        <tbody>
          <tr>
            <td>
<?php
//P函数
function p($arr){
echo '<pre>';
echo print_r($arr);
echo '</pre>';
}
//判断是否为POST提交
if($_SERVER['REQUEST_METHOD']=='POST'){
//	判断是否为上传文件
if(is_uploaded_file($_FILES['up']['tmp_name'])){
//	指定上传的目录
$path='../picture/beijing/';
//	检测上传目录是否存在，不存在则创建(||指前面条件成立则后面不执行，反之则执行)
is_dir($path) || mkdir($path,0777,true);
//	上传文件的类型($_FILES打印出来是数组)
$type=ltrim(strchr($_FILES['up']['type'],'/'),'/');
//	创建文件名
$fileName = time().mt_rand(0, 9999).'.'.$type;
//	创建上传文件路径
$fullpath = $path . $fileName;
//	将上传的文件转移到制定目录下（上传的文件都是临时保存的，不转移系统会自动删除）
move_uploaded_file($_FILES['up']['tmp_name'], $fullpath);
}
}
?>
<!doctype html>
<html lang="en">
<head>
<meta charset="UTF-8" />
</head>
<body>
<!--form表单里面上传文件的时候必须加enctype="multipart/form-data"-->
<form action="" method="post" enctype="multipart/form-data">
<!--隐藏域，name值固定，value设定上传文件大小-->
<!--<input type="hidden" name="MAX_FILE_SIZE" id="" value="30000000" />-->
<!--上传文件input file-->
<input type="file" name="up" id="up" value="" />
<input type="submit" value="上传照片"/>
</form>
<!--显示照片-->
<img src="<?php echo $fullpath ?>"/>
</body>
</html></td>
          </tr>
        </tbody>
      </table>
    </div>
<?php require_once("../footer.php");
?>