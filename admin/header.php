<!--
版权归属:WCST
邮箱:2839472271@qq.com(用@替换#)
如有任何问题欢迎联系!
-->
<?php
$password = '123456'//这里的密码必须和login.php相同
session_start();
//开启session
$password = $_SESSION['password'];
?>
<head>
  <link rel="icon" type="image/x-icon" href="../img/favicon.ico" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <meta charset="utf-8">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/soxft/cdn@master/mdui/css/mdui.min.css">
  <script src="https://cdn.jsdelivr.net/gh/soxft/cdn@master/mdui/js/mdui.min.js"></script>
  <style>
    a {
      text-decoration:none
    }
    a:hover {
      text-decoration:none
    }
  </style>
</head>
  <body background="https://tu.wcst.xyz/apibj.php">
   <?php
   if ($_SESSION['password'] !== $passwd) {
    //判断是否登录
    header("Refresh:0;url=\"./login.php\"");
    exit();
}
?>
    <div class="mdui-tab mdui-tab-full-width mdui-tab-centered">
        <a href="./index.php" class="mdui-ripple">管理首页</a>
        <a href="./txupload.php" class="mdui-ripple">头像上传</a>
        <a href="./bjupload.php" class="mdui-ripple">背景上传</a>
        <a href="./logout.php" class="mdui-ripple">退出登录</a>
        <a href="../index.php" class="mdui-ripple">返回前台</a>
    </div>